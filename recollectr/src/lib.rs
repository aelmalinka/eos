//! Allocator for EOS
#![cfg_attr(not(feature = "std"), no_std)]
#![warn(clippy::pedantic, clippy::nursery, missing_docs)]
#![deny(unsafe_op_in_unsafe_fn)]
#![feature(strict_provenance, let_chains, allocator_api)]
#![cfg_attr(test, feature(vec_into_raw_parts))]

pub(crate) mod traits;

pub mod list;
pub mod util;

extern crate alloc;
