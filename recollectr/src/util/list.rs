//! basic linked list
use alloc::boxed::Box;
use core::cmp::Ordering;
use core::fmt::{self, Debug};
use core::hash::{Hash, Hasher};
use core::iter::FromIterator;
use core::marker::PhantomData;
use core::mem;
use core::ptr::NonNull;

use crate::alloc;

/// linked list
// 2023-07-31 MLT TODO: improve tests
pub struct List<T> {
    front: Link<T>,
    back: Link<T>,
    len: usize,
    _phantom: PhantomData<T>,
}

/// Link between elements in list
pub type Link<T> = Option<NonNull<Node<T>>>;

/// Element in list (including pointers)
#[repr(C)]
pub struct Node<T> {
    elem: T,
    front: Link<T>,
    back: Link<T>,
}

/// ref iter for [`List`]
pub struct Iter<'a, T> {
    front: Link<T>,
    back: Link<T>,
    len: usize,
    _phantom: PhantomData<&'a T>,
}

/// mut ref iter for [`List`]
pub struct IterMut<'a, T> {
    front: Link<T>,
    back: Link<T>,
    len: usize,
    _phantom: PhantomData<&'a mut T>,
}

/// consuming iter for [`List`]
pub struct IntoIter<T> {
    list: List<T>,
}

/// a mut cursor for [`List`]
pub struct CursorMut<'a, T> {
    list: &'a mut List<T>,
    cur: Link<T>,
    index: Option<usize>,
}

impl<T> List<T> {
    #[must_use]
    /// Construct a new empty [`Self`]
    pub const fn new() -> Self {
        Self {
            front: None,
            back: None,
            len: 0,
            _phantom: PhantomData,
        }
    }

    /// push `new` to the front of the list
    /// # Safety
    /// - `new` needs to have valid elem
    /// - `new` needs to have None links
    /// - never call [`Self::pop_front`]/[`Self::pop_back`] on this element
    pub unsafe fn push_front_at(&mut self, new: NonNull<Node<T>>) {
        if let Some(old) = self.front {
            unsafe {
                (*old.as_ptr()).front = Some(new);
                (*new.as_ptr()).back = Some(old);
            }
        } else {
            self.back = Some(new);
        }

        self.front = Some(new);
        self.len += 1;
    }

    /// push `elem` onto front
    pub fn push_front(&mut self, elem: T) {
        let new = unsafe {
            NonNull::new_unchecked(Box::into_raw(Box::new(Node {
                front: None,
                back: None,
                elem,
            })))
        };

        unsafe {
            self.push_front_at(new);
        }
    }

    /// pushes `new` to the back of the list
    /// # Safety
    /// - `new` needs to have valid elem
    /// - `new` needs to have None links
    /// - never call [`Self::pop_front`]/[`Self::pop_back`] on this element
    pub unsafe fn push_back_at(&mut self, new: NonNull<Node<T>>) {
        if let Some(old) = self.back {
            unsafe {
                (*old.as_ptr()).back = Some(new);
                (*new.as_ptr()).front = Some(old);
            }
        } else {
            self.front = Some(new);
        }

        self.back = Some(new);
        self.len += 1;
    }

    /// pushes `elem` to the back
    pub fn push_back(&mut self, elem: T) {
        let new = unsafe {
            NonNull::new_unchecked(Box::into_raw(Box::new(Node {
                back: None,
                front: None,
                elem,
            })))
        };

        unsafe {
            self.push_back_at(new);
        }
    }

    /// # Safety
    /// - you are responsible for reclaiming this link
    pub unsafe fn pop_front_raw(&mut self) -> Link<T> {
        self.front.take().map(|n| {
            self.front = unsafe { n.as_ref().back };
            if let Some(new) = self.front {
                unsafe {
                    (*new.as_ptr()).front = None;
                }
            } else {
                self.back = None;
            }

            self.len -= 1;
            n
        })
    }

    /// pop `elem` off of front or [`None`]
    pub fn pop_front(&mut self) -> Option<T> {
        unsafe { self.pop_front_raw() }.map(|n| {
            let old = unsafe { Box::from_raw(n.as_ptr()) };
            old.elem
        })
    }

    /// # Safety
    /// - you are responsible for reclaiming this link
    pub unsafe fn pop_back_raw(&mut self) -> Link<T> {
        self.back.take().map(|n| {
            self.back = unsafe { n.as_ref().front };
            if let Some(new) = self.back {
                unsafe {
                    (*new.as_ptr()).back = None;
                }
            } else {
                self.front = None;
            }

            self.len -= 1;
            n
        })
    }

    /// pops from the back or `None` if the list is empty
    pub fn pop_back(&mut self) -> Option<T> {
        unsafe { self.pop_back_raw() }.map(|n| {
            let old = unsafe { Box::from_raw(n.as_ptr()) };
            old.elem
        })
    }

    #[must_use]
    /// the first element or [`None`] if list is empty
    pub fn front(&self) -> Option<&T> {
        unsafe { Some(&(*self.front?.as_ptr()).elem) }
    }

    /// the first mut element or [`None`] if list is empty
    pub fn front_mut(&mut self) -> Option<&mut T> {
        unsafe { Some(&mut (*self.front?.as_ptr()).elem) }
    }

    #[must_use]
    /// the last element or [`None`] if list is empty
    pub fn back(&self) -> Option<&T> {
        unsafe { Some(&(*self.back?.as_ptr()).elem) }
    }

    /// the last mut element or [`None`] if list is empty
    pub fn back_mut(&mut self) -> Option<&mut T> {
        unsafe { Some(&mut (*self.back?.as_ptr()).elem) }
    }

    #[must_use]
    /// the length of the list
    pub const fn len(&self) -> usize {
        self.len
    }

    #[must_use]
    /// true if the list contains no elements
    pub const fn is_empty(&self) -> bool {
        self.len == 0
    }

    /// clears the list of all elements
    pub fn clear(&mut self) {
        while self.pop_front().is_some() {}
    }

    #[must_use]
    /// a ref iterator
    pub const fn iter(&self) -> Iter<T> {
        Iter {
            front: self.front,
            back: self.back,
            len: self.len,
            _phantom: PhantomData,
        }
    }

    /// a mut ref iterator
    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut {
            front: self.front,
            back: self.back,
            len: self.len,
            _phantom: PhantomData,
        }
    }

    /// a mut cursor
    pub fn cursor_mut(&mut self) -> CursorMut<T> {
        CursorMut {
            list: self,
            cur: None,
            index: None,
        }
    }
}

impl<T> Node<T> {
    /// Create a new blank node
    pub const fn new(elem: T) -> Self {
        Self {
            front: None,
            back: None,
            elem,
        }
    }

    /// Create a new unattached Link w/ `v` suitable for use w/
    /// `push_{front,back}_at`
    /// # Safety
    /// - start must be properly owned by caller
    pub unsafe fn new_raw(
        start: *mut u8,
        size: usize,
        v: T,
    ) -> Option<(NonNull<Self>, *mut u8, usize)> {
        let offset = start.align_offset(mem::align_of::<Self>());
        if size >= mem::size_of::<Self>() + offset {
            unsafe { NonNull::new(start.add(offset).cast::<Self>()) }.map(|p| {
                unsafe {
                    p.as_ptr().write(Self::new(v));
                }
                (
                    p,
                    unsafe { start.add(offset).add(mem::size_of::<Self>()) },
                    size - offset - mem::size_of::<Self>(),
                )
            })
        } else {
            None
        }
    }

    /// fetch the inner element
    pub const fn elem(&self) -> &T {
        &self.elem
    }

    /// fetch the inner element as a mut ref
    pub fn elem_mut(&mut self) -> &mut T {
        &mut self.elem
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        self.clear();
    }
}

impl<T> Default for List<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T: Clone> Clone for List<T> {
    fn clone(&self) -> Self {
        let mut new = Self::new();
        for item in self {
            new.push_back(item.clone());
        }
        new
    }
}

impl<T> Extend<T> for List<T> {
    fn extend<I: IntoIterator<Item = T>>(&mut self, iter: I) {
        for item in iter {
            self.push_back(item);
        }
    }
}

impl<T> FromIterator<T> for List<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut list = Self::new();
        list.extend(iter);
        list
    }
}

impl<T: Debug> Debug for List<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list().entries(self).finish()
    }
}

impl<T: PartialEq> PartialEq for List<T> {
    fn eq(&self, other: &Self) -> bool {
        self.len() == other.len() && self.iter().eq(other)
    }
}

impl<T: Eq> Eq for List<T> {}

impl<T: PartialOrd> PartialOrd for List<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.iter().partial_cmp(other)
    }
}

impl<T: Ord> Ord for List<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.iter().cmp(other)
    }
}

impl<T: Hash> Hash for List<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.len().hash(state);
        for item in self {
            item.hash(state);
        }
    }
}

impl<'a, T> IntoIterator for &'a List<T> {
    type IntoIter = Iter<'a, T>;
    type Item = &'a T;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.len > 0 {
            self.front.map(|n| unsafe {
                self.len -= 1;
                self.front = (*n.as_ptr()).back;
                &(*n.as_ptr()).elem
            })
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<'a, T> DoubleEndedIterator for Iter<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.len > 0 {
            self.back.map(|n| unsafe {
                self.len -= 1;
                self.back = (*n.as_ptr()).front;
                &(*n.as_ptr()).elem
            })
        } else {
            None
        }
    }
}

impl<'a, T> ExactSizeIterator for Iter<'a, T> {
    fn len(&self) -> usize {
        self.len
    }
}

impl<'a, T> IntoIterator for &'a mut List<T> {
    type IntoIter = IterMut<'a, T>;
    type Item = &'a mut T;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.len > 0 {
            self.front.map(|n| unsafe {
                self.len -= 1;
                self.front = (*n.as_ptr()).back;
                &mut (*n.as_ptr()).elem
            })
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<'a, T> DoubleEndedIterator for IterMut<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.len > 0 {
            self.back.map(|n| unsafe {
                self.len -= 1;
                self.back = (*n.as_ptr()).front;
                &mut (*n.as_ptr()).elem
            })
        } else {
            None
        }
    }
}

impl<'a, T> ExactSizeIterator for IterMut<'a, T> {
    fn len(&self) -> usize {
        self.len
    }
}

impl<T> IntoIterator for List<T> {
    type IntoIter = IntoIter<T>;
    type Item = T;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter { list: self }
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.list.pop_front()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.list.len, Some(self.list.len))
    }
}

impl<T> DoubleEndedIterator for IntoIter<T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.list.pop_back()
    }
}

impl<T> ExactSizeIterator for IntoIter<T> {
    fn len(&self) -> usize {
        self.list.len
    }
}

impl<'a, T> CursorMut<'a, T> {
    /// the current index of the cursor in the list
    #[must_use]
    pub const fn index(&self) -> Option<usize> {
        self.index
    }

    /// the length of the remaining elements
    #[must_use]
    pub fn len(&self) -> usize {
        self.list.len() - self.index.unwrap_or(0)
    }

    /// whether the underlying list is empty
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.list.is_empty()
    }

    /// move to the next element
    /// # Panics
    /// - cur but no index
    pub fn move_next(&mut self) {
        if let Some(cur) = self.cur {
            self.cur = unsafe { (*cur.as_ptr()).back };
            if self.cur.is_some() {
                *self.index.as_mut().unwrap() += 1;
            } else {
                self.index = None;
            }
        } else if !self.list.is_empty() {
            self.cur = self.list.front;
            self.index = Some(0);
        }
    }

    /// move to the next element
    /// # Panics
    /// - cur but no index
    pub fn move_prev(&mut self) {
        if let Some(cur) = self.cur {
            self.cur = unsafe { (*cur.as_ptr()).front };
            if self.cur.is_some() {
                *self.index.as_mut().unwrap() -= 1;
            } else {
                self.index = None;
            }
        } else if !self.list.is_empty() {
            self.cur = self.list.back;
            self.index = Some(self.list.len - 1);
        }
    }

    /// the current element
    pub fn current(&mut self) -> Option<&mut T> {
        self.cur.map(|n| unsafe { &mut (*n.as_ptr()).elem })
    }

    /// peek at the next element
    pub fn peek_next(&mut self) -> Option<&mut T> {
        let next = if let Some(cur) = self.cur {
            unsafe { (*cur.as_ptr()).back }
        } else {
            self.list.front
        };

        next.map(|n| unsafe { &mut (*n.as_ptr()).elem })
    }

    /// peek at the next element
    pub fn peek_prev(&mut self) -> Option<&mut T> {
        let prev = if let Some(cur) = self.cur {
            unsafe { (*cur.as_ptr()).front }
        } else {
            self.list.back
        };

        prev.map(|n| unsafe { &mut (*n.as_ptr()).elem })
    }

    /// split off the elements before current
    /// # Panics
    /// - cur but no index
    pub fn split_before(&mut self) -> List<T> {
        if let Some(cur) = self.cur {
            let old_len = self.list.len;
            let old_idx = self.index.unwrap();
            let prev = unsafe { (*cur.as_ptr()).front };

            let new_len = old_len - old_idx;
            let new_front = self.cur;
            let new_idx = Some(0);

            let len = old_len - new_len;
            let front = self.list.front;
            let back = prev;

            // 2023-07-20 MLT TODO: find optimizations
            if let Some(prev) = prev {
                unsafe {
                    (*cur.as_ptr()).front = None;
                    (*prev.as_ptr()).back = None;
                }
            }

            self.list.len = new_len;
            self.list.front = new_front;
            self.index = new_idx;

            List {
                front,
                back,
                len,
                _phantom: PhantomData,
            }
        } else {
            mem::replace(self.list, List::new())
        }
    }

    /// split off the elements after current
    /// # Panics
    /// - cur but no index
    pub fn split_after(&mut self) -> List<T> {
        if let Some(cur) = self.cur {
            let old_len = self.list.len;
            let old_idx = self.index.unwrap();
            let next = unsafe { (*cur.as_ptr()).back };

            let new_len = old_idx + 1;
            let new_back = self.cur;

            let len = old_len - new_len;
            let front = next;
            let back = self.list.back;

            // 2023-07-20 MLT TODO: find optimizations
            if let Some(next) = next {
                unsafe {
                    (*cur.as_ptr()).back = None;
                    (*next.as_ptr()).front = None;
                }
            }

            self.list.len = new_len;
            self.list.back = new_back;

            List {
                back,
                front,
                len,
                _phantom: PhantomData,
            }
        } else {
            mem::replace(self.list, List::new())
        }
    }

    /// insert list before cursor
    /// # Panics
    /// - cur but no index
    pub fn splice_before(&mut self, mut input: List<T>) {
        if input.is_empty() {
        } else if let Some(cur) = self.cur {
            // 2023-07-20 MLT TODO: can de-dup in_{front,back}?
            let in_front = input.front.take().unwrap();
            let in_back = input.back.take().unwrap();

            if let Some(prev) = unsafe { (*cur.as_ptr()).front } {
                unsafe {
                    (*prev.as_ptr()).back = Some(in_front);
                    (*in_front.as_ptr()).front = Some(prev);
                    (*cur.as_ptr()).front = Some(in_back);
                    (*in_back.as_ptr()).back = Some(cur);
                }
            } else {
                unsafe {
                    (*cur.as_ptr()).front = Some(in_back);
                    (*in_back.as_ptr()).back = Some(cur);
                }
                self.list.front = Some(in_front);
            }

            *self.index.as_mut().unwrap() += input.len;
        } else if let Some(back) = self.list.back {
            let in_front = input.front.take().unwrap();
            let in_back = input.back.take().unwrap();

            unsafe {
                (*back.as_ptr()).back = Some(in_front);
                (*in_front.as_ptr()).front = Some(back);
            }
            self.list.back = Some(in_back);
        } else {
            mem::swap(self.list, &mut input);
        }

        self.list.len += input.len;
        input.len = 0;
    }

    /// insert list before cursor
    /// # Panics
    /// - cur but no index
    pub fn splice_after(&mut self, mut input: List<T>) {
        if input.is_empty() {
        } else if let Some(cur) = self.cur {
            // 2023-07-20 MLT TODO: can de-dup in_{back,front}?
            let in_front = input.front.take().unwrap();
            let in_back = input.back.take().unwrap();

            if let Some(next) = unsafe { (*cur.as_ptr()).back } {
                unsafe {
                    (*next.as_ptr()).front = Some(in_back);
                    (*in_back.as_ptr()).back = Some(next);
                    (*cur.as_ptr()).back = Some(in_front);
                    (*in_front.as_ptr()).front = Some(cur);
                }
            } else {
                unsafe {
                    (*cur.as_ptr()).back = Some(in_front);
                    (*in_front.as_ptr()).front = Some(cur);
                }
                self.list.back = Some(in_back);
            }
        } else if let Some(front) = self.list.front {
            let in_front = input.front.take().unwrap();
            let in_back = input.back.take().unwrap();

            unsafe {
                (*front.as_ptr()).front = Some(in_back);
                (*in_back.as_ptr()).back = Some(front);
            }
            self.list.front = Some(in_front);
        } else {
            mem::swap(self.list, &mut input);
        }

        self.list.len += input.len;
        input.len = 0;
    }

    /// remove the current element returning it's link
    /// # Safety
    /// - you are responsible for reclaiming this link
    /// # Panics
    /// - cur but no index
    pub unsafe fn remove_current_raw(&mut self) -> Link<T> {
        self.cur.map(|mut old| {
            let prev = unsafe { old.as_mut().front.take() };
            let next = unsafe { old.as_mut().back.take() };

            if let Some(prev) = prev {
                unsafe {
                    (*prev.as_ptr()).back = next;
                }
            } else {
                self.list.front = next;
            }

            if let Some(next) = next {
                unsafe {
                    (*next.as_ptr()).front = prev;
                }
            } else {
                self.list.back = prev;
            }

            self.list.len -= 1;
            self.cur = next;
            self.index = next.map(|_| self.index.unwrap() + 1);
            old
        })
    }

    /// remove the current element returning it
    /// this will push the cursor to the next element
    pub fn remove_current(&mut self) -> Option<T> {
        unsafe { self.remove_current_raw() }.map(|n| {
            let old = unsafe { Box::from_raw(n.as_ptr()) };

            old.elem
        })
    }
}

unsafe impl<T: Send> Send for List<T> {}
unsafe impl<T: Sync> Sync for List<T> {}

unsafe impl<'a, T: Send> Send for Iter<'a, T> {}
unsafe impl<'a, T: Sync> Sync for Iter<'a, T> {}

unsafe impl<'a, T: Send> Send for IterMut<'a, T> {}
unsafe impl<'a, T: Sync> Sync for IterMut<'a, T> {}

/// ```compile_fail
/// use recollectr::util::list::IterMut;
///
/// const fn r#mut<'i, 'a, T>(x: IterMut<'i, &'static T>) -> IterMut<'i, &'a T> { x }
/// ```
#[allow(dead_code)]
#[rustfmt::skip]
const fn assert_properties() {
    const fn is_send<T: Send>() {}
    const fn is_sync<T: Sync>() {}

    const fn linked_list_covariant<'a, T>(x: List<&'static T>) -> List<&'a T> { x }
    const fn iter_covariant<'i, 'a, T>(x: Iter<'i, &'static T>) -> Iter<'i, &'a T> { x }
    const fn into_iter_covariant<'a, T>(x: IntoIter<&'static T>) -> IntoIter<&'a T> { x }

    is_send::<List<i32>>();
    is_sync::<List<i32>>();

    is_send::<IntoIter<i32>>();
    is_sync::<IntoIter<i32>>();

    is_send::<Iter<i32>>();
    is_sync::<Iter<i32>>();

    is_send::<IterMut<i32>>();
    is_sync::<IterMut<i32>>();
}

#[cfg(test)]
mod tests {
    use alloc::format;
    use alloc::vec::Vec;
    use core::convert::TryFrom;
    use core::iter;

    use super::*;

    fn generate_test() -> List<i32> {
        list_from(&[0, 1, 2, 3, 4, 5, 6])
    }

    fn list_from<T: Clone>(v: &[T]) -> List<T> {
        v.iter().cloned().collect()
    }

    #[test]
    fn front() {
        let mut list = List::new();
        assert_eq!(list.len(), 0);
        assert_eq!(list.pop_front(), None);
        assert_eq!(list.len(), 0);

        list.push_front(10);
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_front(), Some(10));
        assert_eq!(list.len(), 0);
        assert_eq!(list.pop_front(), None);
        assert_eq!(list.len(), 0);

        list.push_front(10);
        assert_eq!(list.len(), 1);

        list.push_front(20);
        assert_eq!(list.len(), 2);

        list.push_front(30);
        assert_eq!(list.len(), 3);

        assert_eq!(list.pop_front(), Some(30));
        assert_eq!(list.len(), 2);

        list.push_front(40);
        assert_eq!(list.len(), 3);
        assert_eq!(list.pop_front(), Some(40));
        assert_eq!(list.len(), 2);
        assert_eq!(list.pop_front(), Some(20));
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_front(), Some(10));
        assert_eq!(list.len(), 0);
        assert_eq!(list.pop_front(), None);
        assert_eq!(list.len(), 0);
        assert_eq!(list.pop_front(), None);
        assert_eq!(list.len(), 0);
    }

    #[test]
    fn test_basic() {
        let mut m = List::new();
        assert_eq!(m.pop_front(), None);
        assert_eq!(m.pop_back(), None);
        assert_eq!(m.pop_front(), None);
        m.push_front(1);
        assert_eq!(m.pop_front(), Some(1));
        m.push_back(2);
        m.push_back(3);
        assert_eq!(m.len(), 2);
        assert_eq!(m.pop_front(), Some(2));
        assert_eq!(m.pop_front(), Some(3));
        assert_eq!(m.len(), 0);
        assert_eq!(m.pop_front(), None);
        m.push_back(1);
        m.push_back(3);
        m.push_back(5);
        m.push_back(7);
        assert_eq!(m.pop_front(), Some(1));

        let mut n = List::new();
        n.push_front(2);
        n.push_front(3);
        {
            assert_eq!(n.front().unwrap(), &3);
            let x = n.front_mut().unwrap();
            assert_eq!(*x, 3);
            *x = 0;
        }
        {
            assert_eq!(n.back().unwrap(), &2);
            let y = n.back_mut().unwrap();
            assert_eq!(*y, 2);
            *y = 1;
        }
        assert_eq!(n.pop_front(), Some(0));
        assert_eq!(n.pop_front(), Some(1));
    }

    #[test]
    fn test_iterator() {
        let m = generate_test();
        for (i, elt) in m.iter().enumerate() {
            assert_eq!(i32::try_from(i).unwrap(), *elt);
        }
        let mut n = List::new();
        assert_eq!(n.iter().next(), None);
        n.push_front(4);
        let mut it = n.iter();
        assert_eq!(it.size_hint(), (1, Some(1)));
        assert_eq!(it.next().unwrap(), &4);
        assert_eq!(it.size_hint(), (0, Some(0)));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_iterator_double_end() {
        let mut n = List::new();
        assert_eq!(n.iter().next(), None);
        n.push_front(4);
        n.push_front(5);
        n.push_front(6);
        let mut it = n.iter();
        assert_eq!(it.size_hint(), (3, Some(3)));
        assert_eq!(it.next().unwrap(), &6);
        assert_eq!(it.size_hint(), (2, Some(2)));
        assert_eq!(it.next_back().unwrap(), &4);
        assert_eq!(it.size_hint(), (1, Some(1)));
        assert_eq!(it.next_back().unwrap(), &5);
        assert_eq!(it.next_back(), None);
        assert_eq!(it.next(), None);
    }

    #[test]
    #[allow(clippy::manual_next_back)]
    fn test_rev_iter() {
        let m = generate_test();
        for (i, elt) in m.iter().rev().enumerate() {
            assert_eq!(6 - i32::try_from(i).unwrap(), *elt);
        }
        let mut n = List::new();
        assert_eq!(n.iter().rev().next(), None);
        n.push_front(4);
        let mut it = n.iter().rev();
        assert_eq!(it.size_hint(), (1, Some(1)));
        assert_eq!(it.next().unwrap(), &4);
        assert_eq!(it.size_hint(), (0, Some(0)));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_mut_iter() {
        let mut m = generate_test();
        let mut len = m.len();
        for (i, elt) in m.iter_mut().enumerate() {
            assert_eq!(i32::try_from(i).unwrap(), *elt);
            len -= 1;
        }
        assert_eq!(len, 0);
        let mut n = List::new();
        assert!(n.iter_mut().next().is_none());
        n.push_front(4);
        n.push_back(5);
        let mut it = n.iter_mut();
        assert_eq!(it.size_hint(), (2, Some(2)));
        assert!(it.next().is_some());
        assert!(it.next().is_some());
        assert_eq!(it.size_hint(), (0, Some(0)));
        assert!(it.next().is_none());
    }

    #[test]
    fn test_iterator_mut_double_end() {
        let mut n = List::new();
        assert!(n.iter_mut().next_back().is_none());
        n.push_front(4);
        n.push_front(5);
        n.push_front(6);
        let mut it = n.iter_mut();
        assert_eq!(it.size_hint(), (3, Some(3)));
        assert_eq!(*it.next().unwrap(), 6);
        assert_eq!(it.size_hint(), (2, Some(2)));
        assert_eq!(*it.next_back().unwrap(), 4);
        assert_eq!(it.size_hint(), (1, Some(1)));
        assert_eq!(*it.next_back().unwrap(), 5);
        assert!(it.next_back().is_none());
        assert!(it.next().is_none());
    }

    #[test]
    fn test_eq() {
        let mut n: List<u8> = list_from(&[]);
        let mut m = list_from(&[]);
        assert!(n == m);
        n.push_front(1);
        assert!(n != m);
        m.push_back(1);
        assert!(n == m);

        let n = list_from(&[2, 3, 4]);
        let m = list_from(&[1, 2, 3]);
        assert!(n != m);
    }

    #[test]
    fn test_ord() {
        let n = list_from(&[]);
        let m = list_from(&[1, 2, 3]);
        assert!(n < m);
        assert!(m > n);
        assert!(n <= n);
        assert!(n >= n);
    }

    // 2023-07-19 MLT FIXME: replace w/ partial_cmp fix any issues
    #[test]
    #[allow(clippy::many_single_char_names)]
    fn test_ord_nan() {
        let nan = f64::NAN;
        let n = list_from(&[nan]);
        let m = list_from(&[nan]);

        assert_eq!(n.partial_cmp(&m), None);
        assert_eq!(m.partial_cmp(&n), None);

        let n = list_from(&[nan]);
        let one = list_from(&[1.0f64]);
        assert_eq!(n.partial_cmp(&one), None);
        assert_eq!(one.partial_cmp(&n), None);

        let u = list_from(&[1.0f64, 2.0, nan]);
        let v = list_from(&[1.0f64, 2.0, 3.0]);
        assert_eq!(u.partial_cmp(&v), None);
        assert_eq!(v.partial_cmp(&u), None);

        let s = list_from(&[1.0f64, 2.0, 4.0, 2.0]);
        let t = list_from(&[1.0f64, 2.0, 3.0, 2.0]);
        assert_eq!(s.partial_cmp(&t), Some(Ordering::Greater));
        assert!(s > one);
        assert_eq!(s.partial_cmp(&one), Some(Ordering::Greater));
        assert!(s >= one);
    }

    #[test]
    fn test_debug() {
        let list: List<i32> = (0..10).collect();
        assert_eq!(format!("{list:?}"), "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]");

        let list: List<&str> = ["just", "one", "test", "more"].iter().copied().collect();
        assert_eq!(format!("{list:?}"), r#"["just", "one", "test", "more"]"#);
    }

    #[test]
    #[cfg(feature = "std")]
    fn test_hashmap() {
        let list1: List<i32> = (0..10).collect();
        let list2: List<i32> = (1..11).collect();
        let mut map = std::collections::HashMap::new();

        assert_eq!(map.insert(list1.clone(), "list1"), None);
        assert_eq!(map.insert(list2.clone(), "list2"), None);

        assert_eq!(map.len(), 2);

        assert_eq!(map.get(&list1), Some(&"list1"));
        assert_eq!(map.get(&list2), Some(&"list2"));

        assert_eq!(map.remove(&list1), Some("list1"));
        assert_eq!(map.remove(&list2), Some("list2"));

        assert!(map.is_empty());
    }

    #[test]
    fn test_cursor_single_item() {
        let mut m: List<u32> = List::new();
        m.extend([1]);
        let mut cursor = m.cursor_mut();

        cursor.move_next();
        cursor.move_next();
        cursor.move_prev();

        assert_eq!(cursor.current().copied(), Some(1));

        cursor.remove_current();
        let mut l = List::new();
        l.push_back(2);
        cursor.splice_before(l);

        assert_eq!(m.front(), Some(&2));
        assert_eq!(m.back(), Some(&2));
        assert_eq!(m.len(), 1);
    }

    #[test]
    #[allow(clippy::cognitive_complexity)]
    fn test_cursor_move_peek() {
        let mut m: List<u32> = List::new();
        m.extend([1, 2, 3, 4, 5, 6]);
        let mut cursor = m.cursor_mut();
        cursor.move_next();
        assert_eq!(cursor.current(), Some(&mut 1));
        assert_eq!(cursor.peek_next(), Some(&mut 2));
        assert_eq!(cursor.peek_prev(), None);
        assert_eq!(cursor.index(), Some(0));
        cursor.move_prev();
        assert_eq!(cursor.current(), None);
        assert_eq!(cursor.peek_next(), Some(&mut 1));
        assert_eq!(cursor.peek_prev(), Some(&mut 6));
        assert_eq!(cursor.index(), None);
        cursor.move_next();
        cursor.move_next();
        assert_eq!(cursor.current(), Some(&mut 2));
        assert_eq!(cursor.peek_next(), Some(&mut 3));
        assert_eq!(cursor.peek_prev(), Some(&mut 1));
        assert_eq!(cursor.index(), Some(1));

        let mut cursor = m.cursor_mut();
        cursor.move_prev();
        assert_eq!(cursor.current(), Some(&mut 6));
        assert_eq!(cursor.peek_next(), None);
        assert_eq!(cursor.peek_prev(), Some(&mut 5));
        assert_eq!(cursor.index(), Some(5));
        cursor.move_next();
        assert_eq!(cursor.current(), None);
        assert_eq!(cursor.peek_next(), Some(&mut 1));
        assert_eq!(cursor.peek_prev(), Some(&mut 6));
        assert_eq!(cursor.index(), None);
        cursor.move_prev();
        cursor.move_prev();
        assert_eq!(cursor.current(), Some(&mut 5));
        assert_eq!(cursor.peek_next(), Some(&mut 6));
        assert_eq!(cursor.peek_prev(), Some(&mut 4));
        assert_eq!(cursor.index(), Some(4));

        let mut cursor = m.cursor_mut();
        cursor.move_next();
        assert_eq!(cursor.current(), Some(&mut 1));
        assert_eq!(cursor.peek_next(), Some(&mut 2));
        assert_eq!(cursor.peek_prev(), None);
        assert_eq!(cursor.index(), Some(0));
        cursor.move_prev();
        cursor.move_prev();
        assert_eq!(cursor.current(), Some(&mut 6));
        assert_eq!(cursor.peek_next(), None);
        assert_eq!(cursor.peek_prev(), Some(&mut 5));
        assert_eq!(cursor.index(), Some(5));
    }

    #[test]
    fn test_cursor_mut_insert() {
        let mut m: List<u32> = List::new();
        m.extend([1, 2, 3, 4, 5, 6]);
        let mut cursor = m.cursor_mut();
        cursor.move_next();
        cursor.splice_before(iter::once(7).collect());
        cursor.splice_after(iter::once(8).collect());
        // check_links(&m);
        assert_eq!(m.iter().copied().collect::<Vec<_>>(), &[
            7, 1, 8, 2, 3, 4, 5, 6
        ]);
        let mut cursor = m.cursor_mut();
        cursor.move_next();
        cursor.move_prev();
        cursor.splice_before(iter::once(9).collect());
        cursor.splice_after(iter::once(10).collect());
        check_links(&m);
        assert_eq!(m.iter().copied().collect::<Vec<_>>(), &[
            10, 7, 1, 8, 2, 3, 4, 5, 6, 9
        ]);

        let mut cursor = m.cursor_mut();
        cursor.move_next();
        cursor.move_prev();
        assert_eq!(cursor.remove_current(), None);
        cursor.move_next();
        cursor.move_next();
        assert_eq!(cursor.remove_current(), Some(7));
        cursor.move_prev();
        cursor.move_prev();
        cursor.move_prev();
        assert_eq!(cursor.remove_current(), Some(9));
        cursor.move_next();
        assert_eq!(cursor.remove_current(), Some(10));
        check_links(&m);
        assert_eq!(m.iter().copied().collect::<Vec<_>>(), &[
            1, 8, 2, 3, 4, 5, 6
        ]);

        let mut cursor = m.cursor_mut();
        cursor.move_next();
        let mut p: List<u32> = List::new();
        p.extend([100, 101, 102, 103]);
        let mut q: List<u32> = List::new();
        q.extend([200, 201, 202, 203]);
        cursor.splice_after(p);
        cursor.splice_before(q);
        check_links(&m);
        assert_eq!(m.iter().copied().collect::<Vec<_>>(), &[
            200, 201, 202, 203, 1, 100, 101, 102, 103, 8, 2, 3, 4, 5, 6
        ]);
        let mut cursor = m.cursor_mut();
        cursor.move_next();
        cursor.move_prev();
        let tmp = cursor.split_before();
        assert_eq!(m.into_iter().collect::<Vec<_>>(), &[]);
        m = tmp;
        let mut cursor = m.cursor_mut();
        cursor.move_next();
        cursor.move_next();
        cursor.move_next();
        cursor.move_next();
        cursor.move_next();
        cursor.move_next();
        cursor.move_next();
        let tmp = cursor.split_after();
        assert_eq!(tmp.into_iter().collect::<Vec<_>>(), &[
            102, 103, 8, 2, 3, 4, 5, 6
        ]);
        check_links(&m);
        assert_eq!(m.iter().copied().collect::<Vec<_>>(), &[
            200, 201, 202, 203, 1, 100, 101
        ]);
    }

    #[test]
    fn raw() {
        // scratch pad w/ plenty of room
        let block = Vec::<u8>::with_capacity(1024);
        let (start, _, size) = block.into_raw_parts();

        let mut list = List::new();

        unsafe {
            let (p, next, left) = Node::new_raw(start, size, 3).unwrap();
            assert_eq!(p.as_ref().elem, 3);
            list.push_front_at(p);
            let (p, next, left) = Node::new_raw(next, left, 2).unwrap();
            assert_eq!(p.as_ref().elem, 2);
            list.push_front_at(p);
            let (p, next, left) = Node::new_raw(next, left, 1).unwrap();
            assert_eq!(p.as_ref().elem, 1);
            list.push_front_at(p);
            let (p, next, left) = Node::new_raw(next, left, 4).unwrap();
            assert_eq!(p.as_ref().elem, 4);
            list.push_back_at(p);
            let (p, ..) = Node::new_raw(next, left, 5).unwrap();
            assert_eq!(p.as_ref().elem, 5);
            list.push_back_at(p);
        }

        assert_eq!(list.front(), Some(&1));
        assert_eq!(list.back(), Some(&5));
        check_links(&list);

        let mut cur = list.cursor_mut();
        cur.move_next();
        cur.move_next();
        cur.move_next();
        unsafe { cur.remove_current_raw() };
        // 2023-08-20 MLT TODO: more of split testing

        while unsafe { list.pop_back_raw().is_some() } {}
        drop(list);
        let _ = unsafe { Vec::from_raw_parts(start, 0, size) };
    }

    fn check_links<T: Eq + fmt::Debug>(list: &List<T>) {
        let from_front: Vec<_> = list.iter().collect();
        let from_back = list.iter().rev();
        let re_reved: Vec<_> = from_back.rev().collect();

        assert_eq!(from_front, re_reved);
    }
}
