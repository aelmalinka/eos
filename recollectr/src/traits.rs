//! traits
use core::alloc::{AllocError, Allocator, GlobalAlloc, Layout};
use core::marker::PhantomData;
use core::ops::{Deref, DerefMut};
use core::ptr::NonNull;

use lock_api::{Mutex, MutexGuard, RawMutex};

/// # Safety
/// same safety requirements as [`GlobalAlloc`]
pub unsafe trait Alloc {
    unsafe fn alloc(&mut self, layout: Layout) -> *mut u8;
    unsafe fn dealloc(&mut self, data: *mut u8, layout: Layout);
}

/// Represents something that can be locked using critical sections
/// # Safety
/// D: must be thread safe
pub unsafe trait Lockable<T> {
    type D<'a>: DerefMut<Target = T>
    where
        Self: 'a;

    fn lock(&self) -> Self::D<'_>;
}

pub struct Locked<L, A> {
    inner: L,
    _phantom: PhantomData<A>,
}

impl<L, A> Locked<L, A> {
    #[cfg(test)]
    pub const fn new(inner: L) -> Self
    where
        L: Lockable<A>,
        A: Alloc,
    {
        Self {
            inner,
            _phantom: PhantomData,
        }
    }
}

impl<L, A> Deref for Locked<L, A> {
    type Target = L;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<L, A> DerefMut for Locked<L, A> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

unsafe impl<L: RawMutex, A: Alloc> Lockable<A> for Mutex<L, A> {
    type D<'a> = MutexGuard<'a, L, A>
        where L: 'a,
              A: 'a;

    fn lock(&self) -> Self::D<'_> {
        Self::lock(self)
    }
}

// 2023-08-06 MLT TODO: this is probably invalid; doesn't handle ZST
unsafe impl<L: Lockable<A>, A: Alloc> Allocator for Locked<L, A> {
    fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        let r = unsafe { self.alloc(layout) };
        if r.is_null() {
            Err(AllocError)
        } else {
            Ok(unsafe { NonNull::slice_from_raw_parts(NonNull::new_unchecked(r), layout.size()) })
        }
    }

    unsafe fn deallocate(&self, ptr: NonNull<u8>, layout: Layout) {
        unsafe { self.dealloc(ptr.as_ptr(), layout) };
    }
}

unsafe impl<L: Lockable<A>, A: Alloc> GlobalAlloc for Locked<L, A> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        unsafe { self.inner.lock().alloc(layout) }
    }

    unsafe fn dealloc(&self, data: *mut u8, layout: Layout) {
        unsafe { self.inner.lock().dealloc(data, layout) }
    }
}
