//! Linked List Allocator
use core::alloc::Layout;
use core::mem;
use core::ptr::{self, NonNull};

use super::traits::Alloc;
use crate::util::list::{CursorMut, Link, Node};
use crate::util::List;

/// The actual Allocator
#[derive(Clone, Default, Debug)]
pub struct Allocator {
    free: List<Allocation>,
}

#[derive(Copy, Clone, Debug)]
struct Allocation {
    start: *mut u8,
    size: usize,
}

// 2023-08-06 MLT TODO: (safety)
// - UB to unwind *at all*
// - Layout queries and calculations are correct
// - can not rely on allocations actually happening
unsafe impl Alloc for Allocator {
    unsafe fn alloc(&mut self, layout: Layout) -> *mut u8 {
        let mut i = self.suitable(layout);
        while i.next().is_some() {
            if let Some(p) = Self::split(&mut i.cursor, layout) {
                return p;
            }
        }
        ptr::null_mut()
    }

    unsafe fn dealloc(&mut self, free: *mut u8, layout: Layout) {
        // 2023-08-20 MLT TODO: verify good
        unsafe { self.add_free(free, layout.size()) };
    }
}

impl Allocator {
    #[must_use]
    /// Create an empty linked list allocator
    pub fn new() -> Self {
        Self::default()
    }

    /// Create a new Linked List Allocator
    /// # Safety
    /// See [`Self::add_free`]
    pub unsafe fn init(start: *mut u8, size: usize) -> Self {
        let mut ret = Self::default();
        unsafe { ret.add_free(start, size) };
        ret
    }

    /// Add a free range of memory to the allocator
    /// # Safety
    /// - from start to start + size needs to be left to the allocator
    /// # Panics
    /// - if unable to allocate a free marker (with required alignment) in
    ///   `size`
    pub unsafe fn add_free(&mut self, start: *mut u8, size: usize) {
        // 2023-08-21 MLT TODO: this can *not* panic; move allocation to merge
        let alloc = unsafe { Self::allocation(start, size) }.unwrap();
        unsafe { self.merge(alloc) };
    }

    /// Split out a suitable `*mut u8` for `layout`
    fn split(cursor: &mut CursorMut<Allocation>, layout: Layout) -> Option<*mut u8> {
        unsafe { cursor.remove_current_raw() }
            .take()
            .and_then(|mut node| {
                let old = &mut *unsafe { node.as_mut() }.elem_mut();
                let offset = old.start().align_offset(layout.align());

                if offset != 0 {
                    let off = old
                        .start()
                        .align_offset(mem::align_of::<Node<Allocation>>());
                    let size = mem::size_of::<Node<Allocation>>() + off;

                    old.start = unsafe { old.start.add(size) };
                    old.size -= size;

                    Self::splice_before(cursor, unsafe { Self::allocation(old.start(), size) }?);
                }

                let start = old.start();
                let end = unsafe { start.add(layout.size()) };
                let left = old.end() - end.addr();

                if left != 0 {
                    unsafe { cursor.remove_current_raw() };
                    Self::splice_before(cursor, unsafe { Self::allocation(end, left) }?);
                }

                Some(start)
            })
    }

    /// Merge in `alloc` at the correct location
    /// # Safety
    /// - `alloc` is properly constructed and filled
    /// - `alloc` represents only space owned by the allocator
    unsafe fn merge(&mut self, mut alloc: NonNull<Node<Allocation>>) {
        let new = &mut unsafe { alloc.as_mut() }.elem();
        let mut i = self.find_address(new.start());

        if let Some(ref mut prev) = i.peek_prev() && prev.end() == new.start().addr() {
            prev.size += new.size;
        } else {
            Self::splice_before(&mut i, alloc);
        }

        if let Some(cur) = i.current().copied()
            && cur.start().addr() == i.peek_prev().unwrap().end()
        {
            i.peek_prev().unwrap().size += cur.size;
            unsafe { i.remove_current_raw() };
        }
    }

    /// Moves `cur` to be the next block of memory *after* `start`
    fn find_address(&mut self, start: *mut u8) -> CursorMut<Allocation> {
        let mut cur = self.free.cursor_mut();
        cur.move_next();
        while let Some(alloc) = cur.current() && alloc.start().addr() < start.addr() {
            cur.move_next();
        }
        cur
    }

    /// Returns `Suitable` for blocks that are valid for `layout`
    fn suitable(&mut self, layout: Layout) -> Suitable {
        Suitable {
            cursor: self.free.cursor_mut(),
            layout,
        }
    }

    /// Create a valid [`NonNull`]<[`Node`]<[`Allocation`]>>
    unsafe fn allocation(start: *mut u8, size: usize) -> Link<Allocation> {
        unsafe { Node::new_raw(start, size, Allocation { start, size }) }.map(|(p, ..)| p)
    }

    fn splice_before(cursor: &mut CursorMut<Allocation>, alloc: NonNull<Node<Allocation>>) {
        let mut l = List::new();
        unsafe {
            l.push_back_at(alloc);
        }
        cursor.splice_before(l);
    }
}

impl Drop for Allocator {
    fn drop(&mut self) {
        while unsafe { self.free.pop_front_raw() }.is_some() {}
    }
}

struct Suitable<'a> {
    cursor: CursorMut<'a, Allocation>,
    layout: Layout,
}

impl<'a> Iterator for Suitable<'a> {
    type Item = Allocation;

    fn next(&mut self) -> Option<Self::Item> {
        self.cursor.move_next();
        let mut ret = None;
        while let Some(cur) = self.cursor.current().copied() {
            if cur.size >= self.layout.size() {
                ret = Some(cur);
            }
            self.cursor.move_next();
        }
        self.cursor.move_prev();
        ret
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(self.cursor.len()))
    }
}

impl Allocation {
    const fn start(&self) -> *mut u8 {
        self.start
    }

    fn end(self) -> usize {
        self.start().addr() + self.size
    }
}

#[cfg(test)]
mod tests {
    use core::alloc::GlobalAlloc;

    use parking_lot::Mutex;

    use super::*;
    use crate::traits::Locked;

    const ORDINAL: u8 = 42;
    const HEAP_SIZE: usize = 1024;

    type Large = (
        usize,
        usize,
        usize,
        usize,
        usize,
        usize,
        usize,
        usize,
        usize,
        usize,
        usize,
    );

    const LARGE: Large = (
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
        ORDINAL as usize,
    );

    fn block(req: usize) -> (*mut u8, usize) {
        let block = Vec::<u8>::with_capacity(req);
        let (start, _, size) = block.into_raw_parts();
        assert!(size >= req);

        (start, size)
    }

    fn add_memory(size: usize, alloc: &mut Allocator) -> *mut u8 {
        let (start, size) = block(size);
        unsafe { alloc.add_free(start, size) };
        start
    }

    #[test]
    fn suitable_iterator_one() {
        let mut alloc = Allocator::new();
        let p = add_memory(HEAP_SIZE, &mut alloc);

        let layout = Layout::from_size_align(mem::size_of::<u8>(), mem::align_of::<u8>()).unwrap();
        let i = alloc.suitable(layout);

        assert_eq!(layout.size(), 1);
        assert_eq!(layout.align(), 1);
        assert_eq!(i.count(), 1);

        drop(alloc);
        let _ = unsafe { Vec::from_raw_parts(p, 0, HEAP_SIZE) };
    }

    #[test]
    fn split() {
        let mut alloc = Allocator::new();
        let p = add_memory(HEAP_SIZE, &mut alloc);

        let layout = Layout::for_value(&LARGE);
        let mut i = alloc.suitable(layout);

        assert!(i.next().is_some());

        #[allow(clippy::cast_ptr_alignment)]
        let ptr = Allocator::split(&mut i.cursor, layout)
            .unwrap()
            .cast::<Large>();
        unsafe { ptr.write(LARGE) };

        assert_eq!(ptr.cast::<u8>(), p);
        assert_eq!(unsafe { *ptr }, LARGE);

        unsafe {
            alloc.merge(Allocator::allocation(ptr.cast::<u8>(), layout.size()).unwrap());
        }

        assert_eq!(alloc.free.len(), 1);
        assert_eq!(alloc.free.front().unwrap().start(), p);
        assert_eq!(alloc.free.front().unwrap().size, HEAP_SIZE);

        drop(alloc);
        let _ = unsafe { Vec::from_raw_parts(p, 0, HEAP_SIZE) };
    }

    #[test]
    fn add_free_contiguous() {
        let mut alloc = Allocator::new();
        let p = block(HEAP_SIZE * 3);
        let a = (p.0, p.1 / 3);
        let b = (unsafe { a.0.add(a.1) }, a.1);
        let c = (unsafe { b.0.add(b.1) }, a.1);

        assert_eq!(a.0.addr() + a.1, b.0.addr());
        assert_eq!(b.0.addr() + b.1, c.0.addr());

        unsafe { alloc.add_free(a.0, a.1) };
        assert_eq!(alloc.free.len(), 1);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(alloc.free.front().unwrap().end(), a.0.addr() + a.1);
        assert_eq!(alloc.free.back().unwrap().start(), a.0);
        assert_eq!(alloc.free.back().unwrap().end(), a.0.addr() + a.1);

        // 2023-08-20 MLT TODO: todo guaratee order
        unsafe { alloc.add_free(b.0, b.1) };
        assert_eq!(alloc.free.len(), 1);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(alloc.free.front().unwrap().end(), a.0.addr() + a.1 + b.1);
        assert_eq!(alloc.free.back().unwrap().start(), a.0);
        assert_eq!(alloc.free.back().unwrap().end(), a.0.addr() + a.1 + b.1);

        unsafe { alloc.add_free(c.0, c.1) };
        assert_eq!(alloc.free.len(), 1);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(
            alloc.free.front().unwrap().end(),
            a.0.addr() + a.1 + b.1 + c.1
        );
        assert_eq!(alloc.free.back().unwrap().start(), a.0);
        assert_eq!(
            alloc.free.back().unwrap().end(),
            a.0.addr() + a.1 + b.1 + c.1
        );

        drop(alloc);
        let _ = unsafe { Vec::from_raw_parts(p.0, 0, p.1) };
    }

    #[test]
    fn add_free_discontiguous() {
        let mut alloc = Allocator::new();
        let p = block(HEAP_SIZE * 4);
        let a = (p.0, p.1 / 3);
        let b = (unsafe { a.0.add(a.1).add(64) }, a.1);
        let c = (unsafe { b.0.add(b.1).add(64) }, a.1);

        assert_ne!(a.0.addr() + a.1, b.0.addr());
        assert_ne!(b.0.addr() + b.1, c.0.addr());

        unsafe { alloc.add_free(a.0, a.1) };
        assert_eq!(alloc.free.len(), 1);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(alloc.free.front().unwrap().end(), a.0.addr() + a.1);
        assert_eq!(alloc.free.back().unwrap().start(), a.0);
        assert_eq!(alloc.free.back().unwrap().end(), a.0.addr() + a.1);

        unsafe { alloc.add_free(b.0, b.1) };
        assert_eq!(alloc.free.len(), 2);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(alloc.free.front().unwrap().end(), a.0.addr() + a.1);
        assert_eq!(alloc.free.back().unwrap().start(), b.0);
        assert_eq!(alloc.free.back().unwrap().end(), b.0.addr() + b.1);

        unsafe { alloc.add_free(c.0, c.1) };
        assert_eq!(alloc.free.len(), 3);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(alloc.free.front().unwrap().end(), a.0.addr() + a.1);
        assert_eq!(alloc.free.back().unwrap().start(), c.0);
        assert_eq!(alloc.free.back().unwrap().end(), c.0.addr() + c.1);

        drop(alloc);
        let _ = unsafe { Vec::from_raw_parts(p.0, 0, p.1) };
    }

    #[test]
    fn add_free_3way() {
        let mut alloc = Allocator::new();
        let p = block(HEAP_SIZE * 3);
        let a = (p.0, p.1 / 3);
        let b = (unsafe { a.0.add(a.1) }, a.1);
        let c = (unsafe { b.0.add(b.1) }, a.1);

        assert_eq!(a.0.addr() + a.1, b.0.addr());
        assert_eq!(b.0.addr() + b.1, c.0.addr());

        unsafe { alloc.add_free(a.0, a.1) };
        assert_eq!(alloc.free.len(), 1);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(alloc.free.front().unwrap().end(), a.0.addr() + a.1);
        assert_eq!(alloc.free.back().unwrap().start(), a.0);
        assert_eq!(alloc.free.back().unwrap().end(), a.0.addr() + a.1);

        unsafe { alloc.add_free(c.0, c.1) };
        assert_eq!(alloc.free.len(), 2);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(alloc.free.front().unwrap().end(), a.0.addr() + a.1);
        assert_eq!(alloc.free.back().unwrap().start(), c.0);
        assert_eq!(alloc.free.back().unwrap().end(), c.0.addr() + a.1);

        unsafe { alloc.add_free(b.0, b.1) };
        assert_eq!(alloc.free.len(), 1);
        assert_eq!(alloc.free.front().unwrap().start(), a.0);
        assert_eq!(
            alloc.free.front().unwrap().end(),
            a.0.addr() + a.1 + b.1 + c.1
        );
        assert_eq!(alloc.free.back().unwrap().start(), a.0);
        assert_eq!(
            alloc.free.back().unwrap().end(),
            a.0.addr() + a.1 + b.1 + c.1
        );

        drop(alloc);
        let _ = unsafe { Vec::from_raw_parts(p.0, 0, p.1) };
    }

    // #[test]
    // fn alloc() {
    // let alloc = Locked::new(Mutex::new(Allocator::new()));
    // let p = add_memory(HEAP_SIZE, &mut alloc.lock());
    //
    // unsafe {
    // let layout =
    // Layout::from_size_align(mem::size_of::<u8>(),
    // mem::align_of::<u8>()).unwrap(); let ptr = alloc.alloc(layout);
    //
    // assert_ne!(ptr, ptr::null_mut());
    // ptr.write(ORDINAL);
    // assert_eq!(*ptr, ORDINAL);
    //
    // alloc.dealloc(ptr, layout);
    // }
    //
    // assert_eq!(alloc.lock().free.len(), 1);
    // assert_eq!(alloc.lock().free.front().unwrap().size, HEAP_SIZE);
    // drop(alloc);
    // let _ = unsafe { Vec::from_raw_parts(p, 0, HEAP_SIZE) };
    // }
    //
    // #[test]
    // fn single_box() {
    // let alloc = Locked::new(Mutex::new(Allocator::new()));
    // let p = add_memory(HEAP_SIZE, &mut alloc.lock());
    //
    // let b = Box::new_in(ORDINAL, &alloc);
    // assert_eq!(*b, ORDINAL);
    // drop(b);
    //
    // assert_eq!(alloc.lock().free.len(), 1);
    // assert_eq!(alloc.lock().free.front().unwrap().size, HEAP_SIZE);
    // drop(alloc);
    // let _ = unsafe { Vec::from_raw_parts(p, 0, HEAP_SIZE) };
    // }
    //
    // #[test]
    // fn large_box() {
    // let alloc = Locked::new(Mutex::new(Allocator::new()));
    // let p = add_memory(HEAP_SIZE, &mut alloc.lock());
    //
    // let t = (
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // usize::from(ORDINAL),
    // );
    // let b = Box::new_in(t, &alloc);
    // assert_eq!(*b, t);
    // assert_eq!(
    // alloc.lock().free.front().unwrap().size,
    // HEAP_SIZE - mem::size_of::<usize>() * 11
    // );
    // drop(b);
    //
    // let b = Box::new_in(ORDINAL, &alloc);
    // assert_eq!(*b, ORDINAL);
    // assert_eq!(alloc.lock().free.len(), 1);
    // assert_eq!(
    // alloc.lock().free.front().unwrap().size,
    // HEAP_SIZE - mem::size_of::<u8>()
    // );
    // drop(b);
    //
    // assert_eq!(alloc.lock().free.len(), 1);
    // assert_eq!(alloc.lock().free.front().unwrap().size, HEAP_SIZE);
    // drop(alloc);
    // let _ = unsafe { Vec::from_raw_parts(p, 0, HEAP_SIZE) };
    // }
    //
    // #[test]
    // fn single_vec() {
    // let alloc = Locked::new(Mutex::new(Allocator::new()));
    // let p = add_memory(HEAP_SIZE, &mut alloc.lock());
    //
    // let mut v = Vec::new_in(&alloc);
    // v.resize(5, ORDINAL);
    // assert_eq!(format!("{v:?}"), "[42, 42, 42, 42, 42]");
    // assert_eq!(alloc.lock().free.len(), 1);
    // assert_eq!(
    // alloc.lock().free.front().unwrap().size,
    // HEAP_SIZE - mem::size_of::<u8>() * v.capacity()
    // );
    //
    // v.resize(10, ORDINAL);
    // assert_eq!(format!("{v:?}"), "[42, 42, 42, 42, 42, 42, 42, 42, 42, 42]");
    // assert_eq!(alloc.lock().free.len(), 1);
    // assert_eq!(
    // alloc.lock().free.front().unwrap().size,
    // HEAP_SIZE - mem::size_of::<u8>() * v.capacity()
    // );
    //
    // v.resize(5, ORDINAL);
    // assert_eq!(format!("{v:?}"), "[42, 42, 42, 42, 42]");
    // assert_eq!(alloc.lock().free.len(), 1);
    // assert_eq!(
    // alloc.lock().free.front().unwrap().size,
    // HEAP_SIZE - mem::size_of::<u8>() * v.capacity()
    // );
    // drop(v);
    //
    // assert_eq!(alloc.lock().free.len(), 1);
    // assert_eq!(alloc.lock().free.front().unwrap().size, HEAP_SIZE);
    // drop(alloc);
    // let _ = unsafe { Vec::from_raw_parts(p, 0, HEAP_SIZE) };
    // }
}
