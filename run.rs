#!/usr/bin/env cargo
//! ```cargo
//! [package]
//! edition = "2021"
//!
//! [dependencies]
//! clap = { version = "4.3", features = ["derive"] }
//! xshell = "0.1"
//! ```
use clap::Parser;

const SYSTEM: &str = "riscv64";

#[derive(Debug, Parser)]
struct Args {
    target: String,
}

fn main() {
    let Args { target } = Args::parse();

    xshell::cmd!("
        qemu-system-{SYSTEM}
        -machine virt
        -serial mon:stdio
        -nographic
        -kernel {target}
    ").run().unwrap()
}
