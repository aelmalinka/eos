//! Kernel
#![no_std]
#![no_main]
#![warn(clippy::pedantic, clippy::nursery, missing_docs)]
#![deny(unsafe_op_in_unsafe_fn)]
#![feature(naked_functions, custom_test_frameworks)]
#![test_runner(crate::runner)]
#![reexport_test_harness_main = "test_main"]

#[cfg(test)]
extern crate alloc;

#[naked]
#[no_mangle]
#[link_section = ".init.boot"]
unsafe extern "C" fn _boot() -> ! {
    unsafe {
        core::arch::asm!("
            csrw sie, zero
            csrci sstatus, 2

            .option push
            .option norelax
            lla gp, __global_pointer$
            .option pop

            lla sp, __tmp_stack_top

            lla t0, __bss_start
            lla t1, __bss_end

            1:
                beq t0, t1, 2f
                sd zero, (t0)
                addi t0, t0, 8
                j 1b
            2:
                j {}
        ", sym kmain, options(noreturn));
    }
}

extern "C" fn kmain(hart_id: usize, _fdt: *const u8) -> ! {
    let uart_data = 0x1000_0000 as *mut u8;

    if hart_id == 0 {
        for c in b"We're hart 0!\n" {
            unsafe { uart_data.write_volatile(*c) };
        }
    }

    for c in b"Hello, risc-v!\n" {
        unsafe { uart_data.write_volatile(*c) };
    }

    #[cfg(test)]
    test_main();

    hcf()
}

/// testing
pub fn runner(tests: &[&dyn Fn()]) {
    for test in tests {
        test();
    }
}

fn hcf() -> ! {
    loop {
        unsafe { core::arch::asm!("nop") };
    }
}

#[panic_handler]
fn panic(_: &core::panic::PanicInfo) -> ! {
    hcf()
}

#[cfg(test)]
mod tests {
    #[test]
    fn trivial_assert() {
        let a = 2;
        let b = 1 + 1;

        assert_eq!(a, b);
    }
}
